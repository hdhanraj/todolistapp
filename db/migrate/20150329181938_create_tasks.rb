class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :task_name
      t.boolean :done, :default => false
      t.integer :list_id
      t.timestamps
    end
  end
end
