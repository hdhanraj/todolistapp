# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
ToDoListApp::Application.config.secret_key_base = 'c01ffb3b6ed62c545b7d7ff6615936fc0db44a06d279d28bf6f62545d20414f7fd2d40c86fde2730e561d8935b18ce4fa4aa1a0fddbd200b37ebbc5b3376bb58'
