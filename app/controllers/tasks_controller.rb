class TasksController < ApplicationController
  before_action :authenticate_user!
  before_action :set_task, only: [:show, :edit, :update, :destroy]

  # GET /tasks
  # GET /tasks.json
  def index
    # puts "#{current_user.inspect}"
    @tasks = Task.all
    # @pending_tasks = current_user.tasks.where(:done => false)
    # @done_tasks = current_user.tasks.where(:done => true)
    # @users = User.all
    # @followers = current_user.followers
    # puts "followers_ids #{@followers.inspect}"
    # @following = current_user.all_following
    # puts "I am following #{@following}"
  end

  # GET /tasks/1
  # GET /tasks/1.json
  def show
  end

  # GET /tasks/new
  def new
    @task = Task.new
  end

  # GET /tasks/1/edit
  def edit
  end

  # POST /tasks
  # POST /tasks.json
  def create
    @task = Task.new(task_params)

    respond_to do |format|
      if @task.save
        format.html { redirect_to tasks_path, notice: 'Task was successfully created.' }
        format.json { render action: 'show', status: :created, location: @task }
      else
        format.html { render action: 'new' }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tasks/1
  # PATCH/PUT /tasks/1.json
  def update
    respond_to do |format|
      if @task.update(:done => true)
        format.html { redirect_to :back}
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tasks/1
  # DELETE /tasks/1.json
  def destroy
    @task.destroy
    respond_to do |format|
      format.html { redirect_to tasks_url }
      format.json { head :no_content }
    end
  end

  # def follow
  #   @user = User.find(params[:id])
  #   current_user.follow(@user)
  #   redirect_to :back, notice: "You are now following to Harsha"

  # end

  # def unfollow
  #   @user = User.find(params[:id])
  #   current_user.stop_following(@user)
  #   redirect_to :back, notice: "You are not following to Harsha"
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_task
      @task = Task.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def task_params
      params.require(:task).permit(:task_name, :done)
    end
end
