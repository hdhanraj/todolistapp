class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  
  has_many :lists
  
  acts_as_followable
  acts_as_follower

  def self.search(search)
	  if search
	    find(:all, :conditions => ['first_name LIKE ?', "%#{search}%"])
	  else
	    find(:all)
	  end
  end

end
